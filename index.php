


<?php
  /**
   * initialize Hybridauth.
   */
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

  include 'vendor/autoload.php';
  include 'config.php';

  use Hybridauth\Hybridauth;

  $hybridauth = new Hybridauth($config);
  $adapters = $hybridauth->getConnectedAdapters();
?>


<?php if (!$adapters) : ?>
  
  <?php 
    header('Location: https://www.bullcryp.com/?login=no'); 
    exit(0);
  ?>
  
<?php endif; ?>

<?php if ($adapters) : ?> 
    <!-- user is logged in -->
    <script>
        var userProfile = {oauthProviders: [{}]}
        // var token = {}
        // var logoutLink = {}
        <?php $pcount = 0; ?>
        <?php foreach ($adapters as $Name => $adapter) : ?>
            <?php $name = strtolower($Name) ?>
            userProfile.oauthProviders<?php echo '[' . $pcount . ']'; ?> = <?php echo json_encode( $adapter->getUserProfile() ) . "\n\r" ;  ?>
            userProfile.oauthProviders<?php echo '[' . $pcount . ']'; ?>.token = <?php echo json_encode( $adapter->getAccessToken() ) . "\n\r" ;  ?>
            userProfile.oauthProviders<?php echo '[' . $pcount . ']'; ?>.providerName = '<?php echo $name . "' \n\r" ; ?>
            userProfile.oauthProviders<?php echo '[' . $pcount . ']'; ?>.logoutLink = '<?php print $config['callback'] . "?logout={$Name}"; ?>'
            <?php $pcount++ ?>
        <?php endforeach; ?>

    
    </script>


<?php endif; ?>



<!DOCTYPE html>
<html>

<head>
    <title>No Bullcryp Trader</title>

    <link rel="stylesheet" type="text/css" media="screen" href="style2.css">
    <link rel="manifest" href="manifest.json" crossorigin="use-credentials">


    <!-- JQUERY -->
    <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>

    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
        
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.0/css/mdb.min.css" rel="stylesheet">

    <!-- font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous">
    
    <!-- Google Font -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900italic,900' rel='stylesheet' type='text/css'>

    <!-- Favicon -->
    <link rel="icon" href="favicon.ico" sizes="32x32">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-2864028-14"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-2864028-14');
    </script>

</head>

<body>
  
<div id="container">
  <div id="toolbar" class="transition">
      <div class="userpic transition">
        <img id="userpic" class="transition">
      </div>
      
      <div class="navigation" id="nav">
          <div class="navel-wrap">
            <div class="nav_element transition selected" id="Overview" data-sec="overview"><div><img src="./imgs/Overview_icon.png" alt="overview icon"></div><div>Overview</div></div>
            <div class="nav_element transition" id="Trade" data-sec="trade"><div><img src="./imgs/Trade_icon.png" alt="trade icon"></div><div>Trade</div></div>
            <div class="nav_element transition" id="Settings" data-sec="settings"><div><img src="./imgs/Settings_icon.png" alt="settings icon"></div><div>Settings</div></div>
            <div class="nav_element transition" id="Logout"><div><img src="./imgs/power-button.png" alt="logout icon"></div><div id = "logoutlink">Log Out</div></div>            
            <!-- <div class="controls" id="dash_control"><img class="icon_images" src="./imgs/keyboard-right-arrow-button.png"></div> -->
          </div>
        </div>
  </div>
  
  <div id="main" class="transition">
      <!-- Header -->
      <div id="header" class="transition">
          <!-- Greetings -->
          <div class="greeting-wrap transition">
            <h1 class="greeting transition" id="greeting">Welcome back, <span id="username"></span>  </h1>
            <div id="logo" class="transition">
                <img class="logo transition" id="bullcryp_logo" src="./imgs/logo.png" alt="Bullcryp logo">
            </div>
            <h3 class="currPortfolio transition" id="currPortfolio">Your Portfolio Balance is: $2,000,000</h3>
          </div>
          <i class="fas fa-angle-double-left"></i>
      </div>

      <!-- Sections Holder -->
      <div class="sections">
        <!-- Overview Section -->
        <div id="overview" class="section">
          <img src="imgs/overview_screen.png">
        </div>

        <!-- Trade Section -->
        <div id="trade" class="section">
            <img src="imgs/trade_screen.png">
        </div>

        <!-- Settings Section -->
        <div id="settings" class="section">
            <img src="imgs/settings_screen.png">
        </div>
      </div>
  </div>
</div>

</body>




<footer>
  <!-- JQuery -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" defer></script>

  <!-- Explode Animation -->
  <script src="js/jquery.imgexplode.min.js"></script>

  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.min.js" defer></script>

  <!-- Material Design Bootstrap -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.0/js/mdb.min.js" defer></script>

  <!-- Lodash -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.4/lodash.min.js" defer></script>
    
  <script src="js/app.js"></script>
  
</footer>



</html>