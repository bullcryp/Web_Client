

function makePageElements(){
        $("#workspaceElementsPlaceholder").replaceWith(
                makeSettingsBar() +
                makePairTables() +
                
                makeCharts() +
                makeMoversTable() +
               
                makeTradingPanel() +
                makeHoldingsOrdersTables() +
                makeTrendChart() 
                // makeMarginAccount() +
                
                // makeLiveTradeAnalysis() +
                // makeConsole() 
               
        );
        //set tooltips on holdings boxes
        $(".balances.hoverzoom").attr("data-toggle","tooltip").attr("title","Click to select or drag" )
}

function makeSettingsBar(){
    var settings = 
    makeAccordionTop("SETTINGS" ,"settings") + 
    `<select id="settings">
        <option value="binance" label="Binance"></option>
        <option value="polo" label="Poloniex"></option>
    </select>
    <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#settings_modal" onclick="settings.loadSettings()">Settings</button>` +
    makeAccordionBottom()

    return settings
}
function makePairTable(divId, pair) {
    var table;
    table = '<div id="' + divId + '" class="pair ' + pair + '">'
    //  + '<div id = "historybar' + pair + '" class="historybar"> Loading...</div>' 
    table += '<table class="table crytable table-striped table-hover table-condensed">'
    table += '<thead>'
   // table += tableRow('<input v-model="pair">', '{{last}}');
    table += tableRow('<select id="sel' + pair + '" class="pairselect pairmonitor" v-model="pair" v-on:change="changed" v-on:focus="focus" v-on:blur="blur"><option v-for="pr in pairSets" v-bind:value="pr.value">{{pr.text}}</option></select>','{{last}}')  
    table += '</thead>'
    table += '<tbody>'
    table += tableRow('24HR --- low: {{low24hr}} high: {{high24hr}} --- {{range24hr}}%', '', true)
    table += tableRow('Lowest Ask:', '{{lowestAsk}}')
    table += tableRow('Highest Bid:', '{{highestBid}}')
    table += tableRow('24 hr Change:', '{{parseFloat(percentChange).toFixed(4)}}'+' %', false, "v-bind:class='{ greenb: percentChange>0, red: percentChange<0}'")
    table += tableRow('{{base}} Volume:', '{{baseVolume}}')
    table += tableRow('{{coin}} Volume:', '{{quoteVolume}}')
    table += '</tbody></table>'
    table += '</div>'

    return table;
}
function makeChartTimeSet(){
    //<div id='charttimewrap'><input id='charttime' type='text' value='15'></input> Minutes</div>
    return `
    <select id="charttime">
        <option value="1" label="1 Minute"></option>
        <option value="3" label="3 Minutes"></option>
        <option value="5" label="5 Minutes"></option>
        <option value="15" label="15 Minutes" selected="selected"></option>
        <option value="30" label="30 Minutes"></option>
        <option value="45" label="45 Minutes"></option>
        <option value="60" label="1 Hour"></option>
        <option value="120" label="2 Hour"></option>
        <option value="180" label="3 Hour"></option>
        <option value="240" label="4 Hour"></option>
        <option value="D" label="Day"></option>
        <option value="W" label="Week"></option>
    </select>`
}
function makePairTables(){
    return  makeAccordionTop("COIN MONITORS " ,"coinmonitors") + 
            makePairTable(pairsModel.data.pairs[0].name, "pair1") +
            makePairTable(pairsModel.data.pairs[1].name, "pair2") +
            makeCharts(true) +
            makeAccordionBottom()
}
function makeMoversTable(){
    return makeAccordionTop("Market Movers","marketmovers","",true) +
    `
    <div id="movers">
        <div id="moversbanner">Filter for pair: <input v-model="data.filterPair"></div>
        
            <div id="moversWrap">
                <div class="moversrow">
                <div class="moverscount moversdata movershead"></div>
                <div class="movers moversdata movershead">Pair</div>
                <div class="movers moversdata movershead">24 hrs</div>
                <div class="movers moversdata movershead">12 hrs</div>
                <div class="movers moversdata movershead">8 hrs</div>
                <div class="movers moversdata movershead">4 hrs</div>
                <div class="movers moversdata movershead">2 hrs</div>
                <div class="movers moversdata movershead">30 mins</div>
                <div class="movers moversdata movershead">15 mins</div>
                <div class="movers moversdata movershead">5 mins</div>
            </div>
            <div class="moversrow" v-for="row,index in filteredMovers">
                <div class="moverscount">{{index + 1}}</div>
                <div v-for="item in row" :class="{'movers':true,'moversdata':true,'movers-green': item>=0,'movers-red':item<0}">{{item}}</div>
            </div>
        </div>
    </div>
    ` + 
    makeAccordionBottom()
}
function makeTradingPanel(){
    return makeAccordionTop("ORDER BOOK","one","",true) + 
        `<div id="poloO">
            <div id="orderbookWrap">
                <table class="orders-table table-striped table-hover table-condensed">
                    <th>Bids</th>
                    <tr><td></td><td >Price</td><td>Quantity</td><td>Total</td></tr>
                    <tr v-for="item,index in cumSumOrderBook.bids">
                        <td>{{index + 1}}</td><td >{{parseFloat(item[0]).toFixed(8)}}</td><td>{{parseFloat(item[1]).toFixed(8)}}</td><td>{{parseFloat(item[2]).toFixed(8)}}</td>
                    </tr>
                </table>
                <table class="orders-table table-striped table-hover table-condensed">
                    <th>Asks</th>
                    <tr><td></td><td >Price</td><td>Quantity</td><td>total</td></tr>
                    <tr v-for="item,index in cumSumOrderBook.asks">
                        <td>{{index + 1}}</td><td >{{parseFloat(item[0]).toFixed(8)}}</td><td>{{parseFloat(item[1]).toFixed(8)}}</td><td>{{parseFloat(item[2]).toFixed(8)}}</td>
                    </tr>
                </table>
            </div>
            <div style="align-self: baseline; margin-left: 10px;">
                <button onclick="sizeObook(false)" class="sizeholdingsbut">-</button>
                <button onclick="sizeObook(true)" class="sizeholdingsbut">+</button>
            </div>
            <div>Bid Ask Ratio: {{bidAskRatio.toFixed(2)}} to 1<br><br>
            Spread: {{spread.toFixed(4)}} %</div>
            
        </div>` +
    makeAccordionBottom() 
}

function tableRow(left, right, progress, cls) {
    var row = "";
    if (progress) {
        row = '<tr><td colspan="3">';
        row += '    <div class="progress">';
        row += '        <div v-if="low24hr > 0" class="progress-bar" role="progressbar" :aria-valuenow="range24hr" aria-valuemin="0" aria-valuemax="100" :style="{width: range24hr + \'%\',whiteSpace: WS,color: C,paddingLeft: PL}">'; //progress-bar-striped active
        row +=              left;
        row += '        </div><div v-else><img src="images/loading2.gif"></div>';
        row += '    </div></td>';
    } else {
        if(!cls){cls = ""}
        row = '<tr><td>' + left + '</td><td ' + cls + '>' + right + '</td>';
        // if (!noextra) {
            row +='<td style="width:5px"></td>';
        // }
    }
    row += '</tr>';

    return row;
}

function buyAmountTextChange(){
    $('#buyAmtBut').text('Buy $'+$('#buyAmt').val() + " USD" )
}

function sizeObook(grow){
    if(grow){
        // $('#orderbookWrap').css("height","initial")
        if(settings.system.orderbookLength == 5){
            settings.system.orderbookLength += 45
        }else{
            settings.system.orderbookLength += 50
        }
        settings.system.orderbookLength = Math.min(200,settings.system.orderbookLength)
        //set timeout to return orderbook to 5 items
        clearTimeout(growOrdersTimeout)
    }else{
        // $('#orderbookWrap').css("height","120px")
        settings.system.orderbookLength = 5
    }
    updateOrderBook(polo_O.selectedPair)
}
var growOrdersTimeout
function setOrderbookShrinkTimer(){
    clearTimeout(growOrdersTimeout)
    growOrdersTimeout = setTimeout(() => {
        settings.system.orderbookLength = 5
    }, 300000);
}

function makeHoldingsOrdersTables() {
    var account = (
        `<div id="account">` + 
            makeAccordionTop('HOLDINGS <span class="smaller-title">({{data.holdings.length}})</span> -- BALANCE: {{totalBTC}}BTC   VALUE: {{"$" + USDvalue}}USD -- BTC PRICE: {{BTCPrice}} <button class="sizeholdingsbut" onclick="sizeHoldings(true)">-</button><button class="sizeholdingsbut" onclick="sizeHoldings(false)">+</button>','two') +  //<div class="summary">Holdings -- Balance: {{totalBTC}}BTC - Value: {{"$" + USDvalue}}</div>
                `<div class = "buy sortable " id="buy">
                    Buy 
                    <center><div>
                        <ul>
                            <li><select id="buypair" class="pairselect" v-model="data.buyPair"  v-on:change="buyPairChange()"><option v-for="pr in pairSets" :class="['cc ' + getPairCoin(pr.value) ]" v-bind:value="pr.value">{{pr.text}}</option></select></li>
                            <li><button v-on:click="buyCoin('#buyAmt')">Buy Coin..</button>
                            <button v-on:click="buyCoinNow('#buyAmt')">Buy Now</button>
                            <label for="buyAmt">USD value to spend</label>
                            <input id="buyAmt" type="text" value="200"></input>
                            </li>
                        </ul>
                    </div></center>
                </div>
                <div :data = 'fixPair("BTC_" + item[0])' :id = 'fixPair("BTC_" + item[0])' class="balances hoverzoom sortable shadow" v-for="item in data.holdings" >
                    {{item[0]}} <img :src="['./coins/' + item[0].toLowerCase() + '.png']" onerror="this.src='./coins/missing.png'" class="coin"></img>
                        <ul>
                            <li v-if="getCoinPrice(item[0])"> Current Price: <img :src="['./coins/' + (getBaseCoin(item[0])=='BTC'?'usdt.png':'btc.png')]" onerror="this.src='./coins/missing.png'" class="smallcoin"></img> <span v-on:dblclick="item[1].buyat.price = getCoinPrice(item[0])">{{ getCoinPrice(item[0]) }}</span></li>
                            <li><input type="text" v-model.number="item[1].buyat.price" type="number" style="width: 70px"></input> <span :class="calcPercentIncrease(getCoinPrice(item[0]),item[1].buyat.price)>0?'green':'red'">Inc: {{calcPercentIncrease(getCoinPrice(item[0]),item[1].buyat.price)}}%</span></li>
                            <li :class="{ green: item[1].available>0}">Available: {{item[1].available}}
                            <li :class="{ red: item[1].onOrders>0}">On Order: {{item[1].onOrders}}</li>
                            <li>BTC Value: {{item[0]=="USDT"?((parseFloat(item[1].onOrders) + parseFloat(item[1].available))/BTCPrice).toFixed(8):item[1].btcValue}}</li>
                            <li>USD Value: {{item[0]=="USDT"?(parseFloat(item[1].onOrders) + parseFloat(item[1].available)).toFixed(3) :parseFloat(BTCPrice * item[1].btcValue).toFixed(3)}}</li>
                            <li><button v-if="item[0] != 'USDT'" v-on:click="dumpCoin(item[0],parseFloat(item[1].onOrders) + parseFloat(item[1].available),getCoinPrice(item[0]) )">Dump</button>
                                <button v-if="item[0] != 'USDT'" v-on:click="sellAt(item[0],parseFloat(item[1].available),getCoinPrice(item[0]))">Sell at</button>
                                <button v-if="item[0] != 'USDT'" v-on:click="sell(item[0],parseFloat(item[1].available),getCoinPrice(item[0]) )">Sell Now</button>
                                
                            </li>
                        </ul>
                        
                </div>` + 
                makeAccordionTop("LONG TERM","divider","divider",true) + 
                makeAccordionBottom() +
                //`<div id="divider" class="sortable">Long Term</div>` +
            makeAccordionBottom() + 
            makeAccordionTop('ORDERS <span class="smaller-title">({{data.orders.length}})</span>',"three","") +  //<li>{{subItem.date}}</li>  <img class="crypto" :src="['./cryptocoins/SVG/' + getBaseCoin(item[0]) + '.svg']">
                `<div :data = '"o_" + fixPair(item[0])' :id = '"o_" + fixPair(item[0])' class = "orders sortable shadow hoverzoom" v-for="item in data.orders">
                    {{item[0]}} <img :src="['./coins/' + getBaseCoin(item[0]).toLowerCase() + '.png']" class="coin"></img> 
                    <img class="crypto" src="./cryptocoins/exchange.png"> 
                    <img :src="['./coins/' + getPairCoin(item[0]).toLowerCase() + '.png' ]" class="coin"></img><br>
                    
                    Current Price: <img :src="['./coins/' + (getBaseCoin(item[0])=='BTC'?'btc.png':'usdt.png')]" class="smallcoin"></img> {{ getCoinPrice(item[0]) }}
                        <li>
                            <input type="text" v-model.number="item[1].buyat.price" type="number" style="width: 70px"></input> Inc: {{calcPercentIncrease(getCoinPrice(item[0]),item[1].buyat.price)}}%
                        </li>
                        <div v-for="subItem in item[1]" style="float:left">
                            <ul :class="subItem.orderNumber">
                                <li :class="{ red: subItem.type == 'sell','green': subItem.type == 'buy' }">Type: {{subItem.type}}</li>
                                <li><b>Bid: {{subItem.rate}}</b></li>
                                <li>Amount: {{subItem.amount}}</li>
                                <li><b>{{item[0]=='USDT_BTC'? 'USD Value:': getBaseCoin(item[0]) + ' Value:' }} {{subItem.total}}</b></li>
                                <li>
                                    <button class='changeorder' v-on:click="moveOrder(subItem.orderNumber, subItem.rate,subItem.amount, item[0])" :id="subItem.orderNumber">Change...</button>
                                    <button class='cancelorder' v-on:click="cancelOrder(subItem.orderNumber)" :id="subItem.orderNumber">Cancel Order</button>
                                </li>
                            </ul>
                        </div>
                </div>` +  //Margin: {"totalValue":"0","pl":"0","lendingFees":"0","netValue":"0","totalBorrowedValue":"0","currentMargin":"0"},
                
            + makeAccordionBottom() + 
             
           
        `</div></div>` 

        //<div class="hisbarmini">{{makeHistBarNew(item[0])}}</div>
       )
        return account;
        // orders += obj[1][0].date + "<br>"
        //             orders += "Amount: " + obj[1][0].amount + "<br>"
        //             orders += "BTC Value: " + obj[1][0].total + "<br>"
        //             orders += "Type: " + obj[1][0].type 
}


function makeMarginAccount(){
    return makeAccordionTop("MARGIN ACCOUNT","nine","",true) + 
    `<div class = "margin hoverzoom" >
                    Margin Account 
                        <ul>
                            <li>Total Value: {{data.margin.totalValue}}</li>
                            <li>Profit / Loss: {{data.margin.pl}}</li>
                            <li :class="{ red: data.margin.pl < 0,'green': data.margin.pl > 0 }">Gain / Loss: \${{ calcMargin( ((data.margin.netValue-data.margin.totalValue)  *BTCPrice).toFixed(2) ) }}</li>
                            <li>Lending Fees: {{data.margin.lendingFees}}</li>
                            <li>Net Value: {{data.margin.netValue}}</li>
                            <li>Total Borrowed: {{data.margin.totalBorrowedValue}}</li>
                            <li>Current Margin: {{(data.margin.currentMargin*100).toFixed(3)}}%</li>
                        </ul>
                </div>` + 
    makeAccordionBottom()
}
function makeConsole(){
    return makeAccordionTop("CONSOLE","four","",true) + 
    `<h6>Poloniex Responses</h6>
    <div class="console"><center><textarea onmouseenter="document.body.style.overflow='hidden';" onmouseout="document.body.style.overflow='auto'" id="console" rows="12"></textarea></center>` + //v-model:value="data.userData.console.join('\r\n')" v-on:change="saveUser()" 
    makeAccordionBottom()
}
function makeLiveTradeAnalysis(){
    return  makeAccordionTop("LIVE TRADES","five","",true) + 
    `<div class="console"><center><textarea onmouseenter="document.body.style.overflow='hidden';" onmouseout="document.body.style.overflow='auto'" id="server_output" rows="25"></textarea></center>` +
    makeAccordionBottom() 
}
function makeCharts(smallchart){
    if (smallchart){
        return `
            <div class="tradingview-wrapper">
            <div class="tvsym">USDT_BTC</div>
                <!-- TradingView Widget BEGIN -->
                    <div class="tradingview-widget-container">
                    <div id="tv-medium-widget1"></div>
                    <div class="tradingview-widget-copyright"><span class="blue-text"><a href="https://www.tradingview.com/symbols/POLONIEX-BTCUSDT/" rel="noopener" target="_blank"><span class="blue-text">BTCUSDT</span></a></span> <span class="blue-text">Quotes</span> by TradingView</div>
                    <script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
                    </div>
                <!-- TradingView Widget END -->
            </div>
        `

    }else{
        return makeAccordionTop("CHARTING" + makeChartTimeSet(),"seven","") + 
            `<div class="frameholder"><iframe id="if1" class="pairframe"></iframe><div class="framecover"></div></div>`
            // <div class="frameholder"><iframe id="if2" class="pairframe"></iframe><div class="framecover"></div></div>
            + makeAccordionBottom()
    }
}
function makeTrendChart(){
    return makeAccordionTop("TRENDS","eight","",true) 
    + makeAccordionBottom()
}



function makeHistBarNew(pair, mini){

    try{
        var curTime = Math.floor(Date.now() / 1000)
        // var isOld = function(item){return (item.ts > curTime - histBarTimeLength)} 
        // try{var tphO = poloPairs.data.pairHistories.find(x => x.pair == pair).data }catch(e){} //.filter(isOld);
        var pairIndex = poloPairs.data.pairs.findIndex(x => x.name == pair)
        var tph = pairsModel.data.pairTrades.find(x => x.pairModel == pairIndex).data
        var tpl = pairsModel.data.pairTrades.find(x => x.pairModel == pairIndex).liveTrades

        var bar = "", title="", oldestPrice, oldestTime, newestTime
        var mw = "max-width: " + Math.min(390 / tph.length,20) + "px;"

        
        var maxSellVol = 0, maxBuyVol = 0
        
        if(tph.length > 0){
            maxBuyVol = tph.reduce(function(prev,curr){
                return parseFloat(prev.buy.amount-prev.sell.amount) > parseFloat(curr.buy.amount-curr.sell.amount)?prev:curr
            }).buy.amount
            maxSellVol = tph.reduce(function(prev,curr){
                return parseFloat(prev.sell.amount-prev.buy.amount) > parseFloat(curr.sell.amount-curr.sell.amount)?prev:curr
            }).sell.amount
            maxVol = Math.max(maxBuyVol,maxSellVol)
            minValue = tph.reduce(function(prev,curr){
                return parseFloat(prev.close) < parseFloat(curr.close)?prev:curr
            }).low
            maxValue = tph.reduce(function(prev,curr){
                return parseFloat(prev.close) > parseFloat(curr.close)?prev:curr
            }).high
            midValue = (parseFloat(minValue) + parseFloat(maxValue))/2
            midValue = midValue.toFixed(8)
        }
        var red="#f77a7a" //"#f5d1d1"
        var green ="#5fd05f" //"#c9f3c9"
        var backgroundtop = "#eee"
        backgroundbottom = "#ddd"
        var base = pair.split("_")[0]
        var coin = pair.split("_")[1]
        tph.forEach(function (item,index) {
            //height of price line
            var pos = "height:" + ( 100-parseInt(getRangePercent(minValue,maxValue,item.close)) ) + "%;"
            var priceStyle = " style='" + pos + "'"
            //calculate gradient for the top 50%, and bottom 50%, top down
            var buyVol = (50 - parseFloat(getRangePercent(0,maxVol,item.buy.amount - item.sell.amount)*3))
            var sellVol = (50 + parseFloat(getRangePercent(0,maxVol,item.sell.amount - item.buy.amount)*3))
            var ttlVol = parseFloat(item.buy.total - item.sell.total).toFixed(4)
            if(buyVol < 0){buyVol=0};if(buyVol > 50){buyVol=50}
            if(sellVol > 100){sellVol=100};if(sellVol < 50){sellVol=50}
            var volStyle = " background-image: repeating-linear-gradient(to bottom," +
                            backgroundtop + "," +  backgroundtop + " " + buyVol + "%," +
                            green + " " + buyVol + "%," +  green + " 50%," +
                            red + " 50%, " +  red + " " + sellVol + "%," +
                            backgroundbottom + " " + sellVol + "%,"+backgroundbottom+" 100%)"
            
            
            ttlVol = ttlVol < 0? ""+ttlVol:"+"+ ttlVol
            ttlVol = ttlVol + " " + base
            bar = bar + "<div title ='price: " + item.close + " vol: " + ttlVol + "' style='" + mw + volStyle + "'>" + 
                        "<div " + priceStyle  +  "></div></div>"
            
            oldestTime = item.dateStart
            oldestPrice = item.close
            // if(index=0){newestTime = item.dateEnd}
        });
        /** make the live trades bar */
        var liveBar="";
        var totalVolume = 0
        
        if(tpl){
            totalVolume = parseFloat(tpl.buy.total - tpl.sell.total).toFixed(2)
            var liveBuyVol = Math.max(    50 - parseFloat(getRangePercent(0,maxVol,tpl.buy.amount - tpl.sell.amount)*6)    ,0)
            if(liveBuyVol < 0){liveBuyVol=0};if(liveBuyVol > 50){liveBuyVol=50}
            var liveSellVol = Math.max(   50 + parseFloat(getRangePercent(0,maxVol,tpl.sell.amount - tpl.buy.amount)*6)    ,0)
            if(liveSellVol > 100){liveSellVol=100};if(liveSellVol < 50){liveSellVol=50}
            liveBar = "background-image: repeating-linear-gradient(to bottom," +
                            "#f1eeee, #f1eeee " + liveBuyVol + "%," +
                            green + " " + liveBuyVol + "%," +  green + " 50%," +
                            red + " 50%, " +  red + " " + liveSellVol + "%," +
                            "#f1eeee " + liveSellVol + "%,#f1eeee 100%)"

        }
        // console.log("buy: " + tpl.buy.amount + ", sell: " + tpl.sell.amount + ", buy % : " + liveBuyVol + ", sell % : " + liveSellVol)
        
        

        if(!mini){
            bar = `<div class="livetrades" title="` + totalVolume + " " + base + `" style="`+ liveBar +`"></div></div><div class='hist-overlay'><div class='hist-upper'><div class="overlay-value">` + maxValue + `</div><div class='hist-mid'><div class="overlay-value">` + midValue + `</div><div class='hist-lower'></div></div></div></div>` + 

                "<div class='hist-inner'>" + bar + "</div><div class='hist-stats'><div class='overlay-value'>" + minValue + "</div><center><div style='margin-top: -14px;'> " + secsToTimeString(parseInt( curTime - moment(oldestTime).unix() ) ) + " --- " + (tph.length / parseInt((curTime - moment(oldestTime).unix())/60)).toFixed(2) + " tpm -- " + tph.length + "</div></center></div>" 
        }
        return bar
    }catch(e){
        // console.log(e)
        return ""
    }
    

    // background-image: repeating-linear-gradient(to bottom, grey, grey 25%, red 25%, red 50%, black 50%,black 75%, grey 75%, grey 100%);



// Object
// buy: amount:5.217564279999998
        // price:0.14004997
        // total:0.73074022
        // trades:14
// close:2520.317
// dateEnd:"2017-06-19T00:29:41.000Z"
// dateStart:"2017-06-19T00:29:16.000Z"
// endTradeId:4897179
// high:2520.317
// low:2520.3
// sell:amount:5.217564279999998
        // price:0.14004997
        // total:0.73074022
        // trades:14
// startTradeId:4897172



}

// function openAccordionGroup(title, num, toggleGroup,closed){
//    return  ` <div class="panel-group" id="accordion">` + 
//    makeAccordionTop(title, num,closed)
// }

function makeAccordionTop(title, id , clAss , closed){
    var tg;
    if(closed){closed=""}else{closed=" in"}
    // if(!toggleGroup){tg = "#accordion"}
   return  `<div class="panel panel-default `+ clAss +`" id="` + id + `">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a class="accordion-toggle" data-toggle="collapse"  href="#collapse` + id + `">`
          + title +
       ` </a>
      </h4>
    </div>
    <div id="collapse` + id + `" class="panel-collapse collapse` + closed + `">
      <div class="panel-body" style="padding: 0">`  
}

function makeAccordionBottom(){
    return `</div>
          </div>
        </div>`
}

// function closeAccordionGroup(){
//     return makeAccordionBottom() + `</div></div>`
// }

function getRangePercent(min,max,value){

        return ((value - min) * 100) / (max - min)
}
