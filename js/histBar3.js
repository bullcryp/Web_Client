function makeHistBar3(pair){
    var timeLength = 600; // 10 mins - max time duration on bar
    var curTime = Math.floor(Date.now() / 1000);
    var isOld = function(item){return (item.ts > curTime - timeLength)}
    var h = poloPairs.data.pairHistories.find(x => x.pair == pair).data.slice(0,100).filter(isOld);
    var bar = "", title="", cls="", lastPrice;
    var mw = "max-width: " + 370 / h.length + "px;";

    

    var minValue = h.reduce(function(prev,curr){
        return prev.ls < curr.ls?prev:curr
    }).ls
    var maxValue = h.reduce(function(prev,curr){
        return prev.ls > curr.ls?prev:curr
    }).ls

    $(h).each(function (index, item) {
        var showTitle = true
        var pos = "top:" + ( 100-parseInt(getRangePercent(minValue,maxValue,item.ls)) ) + "%;"
        var style = " style='height: 3px;width:10px;background-color:black;" + pos + mw + "'" 
        // if(showTitle){title = " title='" + item.ls + " pi: "}else{title=""}
        bar = bar + "<div style='height:100px;width:100%; '><div" + style  +  "'></div></div>"
        lastPrice = item.ls
    })
    bar = "<div>" + bar + "</div><div>" + lastPrice + "</div>"

    return bar
 
    
}

function getRangePercent(min,max,value){
        return ((value - min) * 100) / (max - min)
    }