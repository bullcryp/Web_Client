
var toolbarMin = false

const init = ()=>{

  $(".navel-wrap").mouseenter(function(e){
    setToolbar(true)
  }).mouseleave(function(e){
    setToolbar(false)
  })

  $("#main").mouseenter(function(e){
    setToolbar(false)
  })

  $(".nav_element").click(function(e){
    doNav(this)
  })
  $("#logo").mouseenter(function(e){
    e.stopPropagation();
  })

  //create logout link
  $('#Logout').click( function(){
    location.href = userProfile.oauthProviders[0].logoutLink
  })
  //$('#logoutlink').html(userProfile.oauthProviders[0].logoutLink)

  //create user pic
  $("#userpic").attr("src", userProfile.oauthProviders[0].photoURL)

  //append username
  $("#username").text(userProfile.oauthProviders[0].firstName)

  const setToolbar = (enter)=>{
    if(toolbarMin){return}
    if(enter){
      $("#toolbar").css('width', "175px")
      $("#main").css('padding-left', '105px')
      $(".navigation .nav_element div:last-child").css('display','block')
      setTimeout(() => {
        $(".navigation .nav_element div:last-child").animate({opacity:1},500)
      }, 250);
    }else{
      $("#toolbar").css('width', "70px")
      $("#main").css('padding-left', '0px')
      $(".navigation .nav_element div:last-child").css('display','none')
      $(".navigation .nav_element div:last-child").css('opacity', '0')
    }
  }

  const doNav = (el)=>{
    let sec
    if( !$(el).is("#Logout") ){
      $(".nav_element").removeClass("selected")
      sec = "#" + $(el).data("sec")

      // Change main area section
      $(".section").fadeOut()
      $(sec).fadeIn("slow")
    }
    $(el).not("#Logout").addClass("selected")
  }
  // Toggle nav height
  $('.fas').click(function(e){
    let head = $("#header")
    if(head.data("size")=='S'){
      toolbarMin = false
      head.data("size","L")
      head.css('height','194px')
      $("#logo").css("top","36px")
      $("#logo img").css("width","8vw")
      $(".greeting-wrap").css("padding","30px 0")
      $(".greeting").css("font-size", "5em")
      $(".currPortfolio").css("font-size", "2em")
      $("#toolbar").css("width","70px")
      $("#container").css("grid-template-columns","70px auto")
      $("#toolbar div:not(:first)").fadeIn("slow")
      $(".navigation .nav_element div:last-child").css('display','none')
      $(".navigation .nav_element div:last-child").css('opacity', '0')
      $('.userpic').css("margin-left","initial").css("margin-top","initial").css("position","fixed")
      $("#userpic").css("width","initial")
      
    }else{
      toolbarMin = true
      head.data("size","S")
      head.css('height','50px')
      $("#logo").css("top","4px")
      $("#logo img").css("width","3vw")
      $(".greeting-wrap").css("padding","10px 0")
      $(".greeting").css("font-size", "1em")
      $(".currPortfolio").css("font-size", ".75em")
      $("#toolbar").css("width","20px")
      $("#container").css("grid-template-columns","20px auto")
      $("#toolbar div:not(:first)").fadeOut("slow")
      $('.userpic').css("margin-left","100px").css("margin-top","5px").css("position","static")
      $("#userpic").css("width","40px")
    }

  })

  // service worker
  if('serviceWorker' in navigator){
    try {
      navigator.serviceWorker.register('appsw.js')
      console.log('SW Registered')
    }catch(e){ 
      console.log(e) 
    }
  }

}

init()