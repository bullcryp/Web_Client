//import { settings } from "cluster";



$(document).ready(function () {

    if(typeof initComplete == "undefined"){
        getScripts(initialize)  //load scripts, with the initialize function as the callback when done
    }else{
        initialize()
    }

    //reload the page every hour
    // setInterval(function(){
    //     window.location.reload(true);
    // },60*60000)
    
})

function initialize(){
    flash = jQuery(".data")
                
    //in vuemodels
    makePairModel()                

    makePageElements()

    makeAccountingModels()
    
    makePairViews()  
    
    // setUpTroll()

    //load some settings
    loadSettings()

    setCharts()

    sendInitializationCommansToServer()

    finalizeUI()

    initComplete = true
    
}

function finalizeUI(){

    $("#startup_output").remove()

    // final setup, as yet uncategorized
    $(".framecover").click(function(){
            // $(this).slideToggle("slow").delay(10000).slideToggle("slow")
            if($(this).hasClass("toggled")) {
                $(this).animate({"height": "300px"}).removeClass("toggled")
                .css("background-color" ,"rgba(0,0,0,0.07)")
              } else {
                $(this).animate({"height": "17px"}).addClass("toggled")
                .css("background-color", "rgba(0,0,0,0.3)")
                setTimeout(() => {
                    $(this).animate({"height": "300px"}).removeClass("toggled")
                    .css("background-color" ,"rgba(0,0,0,0.07)")
                }, 60000);
            }
    })

    

    $(".pair1").css({'border': '3px solid blue', 'box-shadow': 'none'})

    $(".frameholder").addClass("shadow")
    $(".pair").addClass("shadow")

    $(".data_container").appendTo(".panel-title:first")



    // $(".pairselect").each(function(index){
    //     $(this).change(function(){
    //         account.buyPairChange()
    //     })
    $("#selpair1").chosen({search_contains: true, display_disabled_options:false}).change(function(){
        pair1Model.pair = $("#selpair1").val()
        setBuyPair(pair1Model.pair)
    })
    $("#selpair2").chosen({search_contains: true, display_disabled_options:false}).change(function(){
        pair2Model.pair = $("#selpair2").val()
        setBuyPair(pair2Model.pair)
    })
    $("#selpair3").chosen({search_contains: true, display_disabled_options:false}).change(function(){
        pair3Model.pair = $("#selpair3").val()
        setBuyPair(pair3Model.pair)
    })
    $(".pair").click(function(e){
        //highlight this pairMonitor
        $(".pair").css({'border': 'none','box-shadow': '2px 2px 14px 0px rgba(0,0,0,0.25)'})
        $(this).css({'border': '1px solid blue', 'box-shadow': 'none'})

        //if this is a human clicked event...
        if(e.originalEvent !== undefined){
            doPairClick(this)
        }
    })
    function holdingsClicks(){
        $(".balances.hoverzoom").each(function(index,item){
            $(item).unbind()
            $(item).click(function(e){

                //highlight this holding
                $(".balances.hoverzoom").css({'border': 'none','box-shadow': '2px 2px 14px 0px rgba(0,0,0,0.25)'})
                $(this).css({'border': '1px solid blue', 'box-shadow': 'none'})

                //if natively clicked, call pairClick
                if(e.originalEvent !== undefined){
                    doPairClick(this)
                    // $("#account").find("#h_"+value).trigger("click")
                }

            })
        })
        $(".orders.hoverzoom").each(function(index,item){
            $(item).unbind()
            $(item).click(function(e){
                //highlight this order
                $(".orders.hoverzoom").css({'border': 'none','box-shadow': '2px 2px 14px 0px rgba(0,0,0,0.25)'})
                $(this).css({'border': '1px solid blue', 'box-shadow': 'none'})

                //if natively clicked, call pairClick
                if(e.originalEvent !== undefined){
                    doPairClick(this)
                    // $("#account").find("#"+value).trigger("click")
                }
            })
        })
    }
    function doPairClick(ths){
        //set the clicked pair, and coin
        var pair = $(ths).attr("id")
        var orderPair = "h_" + pair
        var coin = pair.split("_")[1]

        
        
        //send to server which orderbook to refresh
        // sendToServer('account.getorderbook',pair)

        //set value to current pair
        var value = pair //this.attributes[0].nodeValue
        if(value == "BTC_BTC" || value == "BTC_USDT"){
            value = "USDT_BTC"
        }
        //set trends search
        $('#trends-widget-1').attr('src',
            `https://trends.google.com:443/trends/embed/explore/TIMESERIES?req=%7B%22comparisonItem%22%3A%5B%7B%22keyword%22%3A%22`+ coin +`%22%2C%22geo%22%3A%22%22%2C%22time%22%3A%22now%207-d%22%7D%5D%2C%22category%22%3A0%2C%22property%22%3A%22%22%7D&tz=480&eq=date%3Dnow%207-d%26q%3D` + coin
        )
        //set buy pair control
        // setBuyPair( pair )

        //set pairModel pair, and set pairMonitor and pair selector
        var pairNum = "3"
        var pairM = pair3Model
        if( $("#selpair1").val() == value){
            pairNum = "1"
            pairM = pair1Model
        }
        if( $("#selpair2").val() == value){
            pairNum = "2"
            pairM = pair2Model
        }
        
        $("#selpair" + pairNum).val(value);
        $("#selpair" + pairNum).trigger("chosen:updated")
        pairM.pair = value

        $(".pair" + pairNum).attr("id", value)
        
        
        if(ths != $(".pair" + pairNum)[0]){
            $(".pair" + pairNum).trigger("click")
        }
        //click each object, if not native
        if(ths != $("#account").find("#"+pair)[0]){ //if not same holding
            $("#account").find("#"+pair).trigger("click")
        }
        if(ths != $("#account").find("#h_"+pair)[0]){ //if not same order
            $("#account").find("#h_"+pair).trigger("click")
        }

        //set vue model pair
        polo_O.selectedPair = pair


    }
    function getPairTablePair(pair){

    }

    // $(".pairmonitor").each(function () {
    //     var $self = $(this);
    //     $self.data("previous_value", $self.val());
    // });

    // $(".pairmonitor").on("change", function () {
    //     var $self = $(this);
    //     var prev_value = $self.data("previous_value");
    //     var cur_value = $self.val();

    //     var conflict = false
    //     $(".pairmonitor").not($self).each(function () {
    //         if ( $(this).val() == cur_value){
    //             conflict = true
    //         }
    //     })

    //     if(conflict){
    //         $self.val(prev_value)
    //         return false
    //     }

    // //     if (cur_value != "") {
    // //         $(".pairmonitor").not($self).find("option").filter(function () {
    // //             return $(this).val() == cur_value;
    // //         }).prop("disabled", true);

    // //         $self.data("previous_value", cur_value);
    // //     }
    //     // $('.pairmonitor').trigger('chosen:updated');
    // });
    // $('.pairmonitor').trigger('chosen:updated');

    $(document).keypress(function(event){
        if(event.shiftKey){
            if(event.charCode == 78){ //shift-n
                $(".buy button:nth-child(2)").trigger("click")
            }
            if(event.charCode == 66){ // shift-b
                $(".buy button:first").trigger("click")
            }
        }
    })
    
    setTimeout(() => {
        holdingsClicks()
        $(".balances.hoverzoom").css({'border': 'none','box-shadow': '2px 2px 14px 0px rgba(0,0,0,0.25)'})

        $(".chosen-container").on("click", function(e){
            e.stopPropagation()
        })
        
        // $('#account .panel-body:first-child').masonry({
        //     // options
        //     itemSelector: '.balances.hoverzoom',
        //     columnWidth: 198
        //   });
    }, 2000);

    function setBuyPair(value){
        if(value=="BTC_BTC" || value == "BTC_USDT"){
            value = "USDT_BTC"
        }
        $('#buypair').val(value);
        $('#buypair').trigger("chosen:updated");
        account.buyPairChange()
        flashItem(".buy", "lightgreen")
    }

    $("#buypair").chosen({search_contains: true}).change(function(){
        account.buyPairChange()
    })
    // })
    // setTimeout(function(){
    //     var textarea = document.getElementById('console')
    //     textarea.scrollTop = textarea.scrollHeight
    // },5000)
}

function loadSettings(){
    if(accountModel){
        accountModel.data.holdings = getLs("balances",true)
        accountModel.data.holdings.forEach(function(e){
            if(!e[1].buyat){
                e[1].buyat = {pair:e[0],price:0}
            }
        })
        accountModel.data.orders = getLs("orders",true)
        accountModel.data.orders.forEach(function(e){
            if(!e[1].buyat){
                e[1].buyat = {pair:e[0],price:0}
            }
        })
        // accountModel.data.userData.userName = defaultUsername
        // sendToServer("loadUser", accountModel.data.userData.userName)
    }

    
}

function setCharts(){
    /** load the tradingview charts, after 10 - 20 seconds of page loading */
    setTimeout(()=>{ $('#if1').attr('src', 'pair_graph.html?pair=' + pair1Vue.polo_name) },5000)
    setTimeout(()=>{ $('#if2').attr('src', 'pair_graph.html?pair=' + pair2Vue.polo_name) },7500)
    setTimeout(()=>{ $('#if3').attr('src', 'pair_graph.html?pair=' + pair3Vue.polo_name) },10000)

    setTimeout(() => {
        jQuery("#trends-widget-1").appendTo("#collapseeight div.panel-body")
    }, 3000);

    // setTimeout(() => {
    //     $('#trends-widget-1').attr('src',`
    //     https://trends.google.com:443/trends/embed/explore/TIMESERIES?req=%7B%22comparisonItem%22%3A%5B%7B%22keyword%22%3A%22bitcoin%22%2C%22geo%22%3A%22%22%2C%22time%22%3A%22now%207-d%22%7D%5D%2C%22category%22%3A0%2C%22property%22%3A%22%22%7D&tz=480&eq=date%3Dnow%207-d%26q%3Dbitcoin
    //     `)
    // }, timeout);
}

function sendInitializationCommansToServer(){
    //  TODO send pairsets..
    //  send other..
    
    // function getOrderBookMeth(){
    //     sendToServer('account.getorderbook','all')
    // }
    // setIntervalAndCall(getOrderBookMeth,60000,true)

    // function tradeStreamPairsMeth(){
    //     sendToServer('tradestreampairs',getTradeStreamPairs())
    // }

    // setIntervalAndCall(tradeStreamPairsMeth,5000,false, false)

    // sendToServer('account.getorderbook',getTradeStreamPairs()[0])
    
}


function setIntervalAndCall(method, interval, immediate, once){
    if(immediate){method()}
    if(!once){
        setInterval(function(){
            method()
        },interval)
    }else{
        setTimeout(function(){
            method()
        },interval)
    }
}

function getScripts(callback){
    var script_arr = [
                        "lib/vue",
                        "lib/socket.io",
                        "lib/autobahn.min",
                        "global_constants",
                        "analysis_functions",
                        "web_functions",
                        "storage",
                        "vuemodels",
                        "sio",
                        "trollbox",
                        "autobahn_subscribe",
                        "makeHTMLElements",
                        "pkeys",
                        "linkify/linkify.min",
                        "linkify/linkify-html.min",
                        "https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js"
                     ]
    processScriptArray(script_arr,callback)
}

function processScriptArray(script_arr,callback){
    var scrpt;
    if(script_arr[0].includes("http")){
        scrpt = script_arr[0]
    }else{
        scrpt = "/js/" + script_arr[0] + ".js"
    }
    $.getScript(scrpt)
        .done(()=>{ 
                    $("#startup_output").val( $("#startup_output").val() + "loaded " + script_arr[0] + "\n")
                    setTimeout(function(){
                        script_arr.shift()  //remove first element from scripts array
                        if(script_arr.length){
                            processScriptArray(script_arr,callback)
                        }else{
                            // $("#startup_output").val( $("#startup_output").val() + "\nStarting Up.....")
                            callback()
                        }
                    },25)
                    
                  }
        )
        .fail(()=>{ 
                    //if script loading fails, processing array stops
                    $("#startup_output").val( $("#startup_output").val() + "failed to load " + script_arr[0] + "\n\n**Startup Halted**") 
                  })
}

