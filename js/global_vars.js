/***global variables */

//flag to use for script loader knowing if we've already loaded scripts in index file for debugging
var initComplete = false


var settings = {
    account: {
        user: {
            firstname: "",
            lastname: "",
            photo: "",
            Emal: "",
            interface: {}
        },
        subscription:{
            brand: "",
            created: 23456789,
            id: "",
            receiptNumber: "",
            subscriptionLevel: {},
            features: {},
        }
    },
    exchanges: {
        poloniex: {
            name: "poloniex",
            key: "",
            secret: ""
        },
        binance:{
            name: "binance",
            key: "",
            secret: ""
        },
    },
    currentState: {
        orderbookVisible: true,
        marketMoversVisible: true
    },
    system: {
        orderbookLength: 5,
        dataPairHistoryLength: 10,
        ajaxTimeout: 25000,
        initComplete: false,
        sellFactor: .85,                //percent range between high bid and low ask
        buyNowFactor: 1.25,
        dumpFactor: .75,
        holdingsCount: null,
        ordersCount: null
    },
    crypto:{
        publickey:null,
    },
    saveSettings: ()=>{
        settings.exchanges.poloniex.key = jQuery("#polokey").val()
        settings.exchanges.poloniex.secret = jQuery("#polosec").val()

        setLs("polokeys", {key: settings.exchanges.poloniex.key, secret: settings.exchanges.poloniex.secret},true,true)
    },
    loadSettings: ()=>{
        let polokeys = getLs("polokeys",true,true)
        try {
            settings.exchanges.poloniex.key = polokeys.key
            settings.exchanges.poloniex.secret = polokeys.secret 
        
            jQuery("#polokey").val(settings.exchanges.poloniex.key)
            jQuery("#polosec").val(settings.exchanges.poloniex.secret)
        }catch(e){ }
        
        return true;
      }
}

settings.system.currentExchange = settings.exchanges.poloniex


//global flash object for data input 
var flash;


/**Pair Vue Models Enum */
const pairModelEnum = {
    pair: 0,
    last: 1,
    lowestAsk: 2,
    highestBid: 3,
    percentChange: 4,
    baseVolume: 5,
    quoteVolume: 6,
    isFrozen: 7,
    low24hr: 8,
    high24hr: 9,
    range24hr: 10,
    WS: 11,
    C: 12,
    PL: 13,
    historyBarLength: 14,
    sameCount: 15
}