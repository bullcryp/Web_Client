
//handle all trollbox events and save chats to disk
function procTrollEvent(args) {
        // console.log(args[2],args[4]);
        showData();


        scrolledBottom = isScrolledToBottom("trollinner");
        var myname = "", reputation = "", banhammer = "",
                mods = trollboxMods

                //ones to watch
                //      logictrading
                //      PicklePirate
                //      SwordsMan
                //      HotmetalRISE CryptoMind BTC Pumper?

        if (args[3].includes(trollBoxName)) {
                myname = " color:green;font-weight:bold"
                // beep("notification-up.wav");
        }

        if (mods.includes(args[2])) {
                reputation = " style='color:blue' ";
        } else if (args[4] > 399) {
                reputation = " style='color:red' ";
        }
        if (args[2] == "Banhammer") {
                banhammer = "<div class='banhammer'><img src='images/banhammer.gif'></div>"
                // beep();
        }
        var display = "";
        if (!scrolledBottom || ($("#trollsearch").val() != "" && containsTerms(args[2] + args[3])==false) ) {
                display = " display:none"
        }

        //linkify the chat item
        try{
                args[3] = linkifyHtml(args[3])
        }catch(e){}

        if (args[2].includes("marking@")) {
                trollbox.append("<div style='" + display + "' class='marked'>" + args[3] + "</div>")
        } else {
                trollbox.append("<div style='" + myname + display + "'><div><span class='time'>" + new Date(new Date().getTime()).toLocaleTimeString() + "</span><strong" + reputation + ">" + args[2] + ": </strong>" + args[3] + "</div>" + banhammer + "</div>")
        }

        

        var comments = trollbox.children("div").length;
        var scMst = trollbox.scrollHeight - trollbox.scrollTop  //needs to get bigger when removing el.
        if (doEvery(20)){
                while (comments > 1000) {
                        var div = trollbox.find('div').first();
                        // var divH = div.height();
                        // var visible = $(div).css("display")!="none"
                        div.remove();
                        comments -= 1;
                        // if (!scrolledBottom && visible) {
                        //         // trollbox.scrollTop(trollbox.scrollTop() + divH);
                        // }
                }
        
                //save the chats maybe 1 in 20 times
                setLs("chats", $("#trollinner").html());
        }

        // $("#trollsearch").trigger("change");

        if (scrolledBottom) {  //scroll to bottom, if it was before we added elements
                scrollToBottom();
        }
}

function containsTerms(value){
        terms = trollSearch.trim().split(" ");
        $.each(terms, function(index,val){
                if (val == value){
                        return true
                }
        })
}


function isScrolledToBottom(el) {
        var ele = document.getElementById(el);
        // console.log("scroled bottom: " + ele.scrollHeight - ele.scrollTop == jQuery("#" + el).height())
        return ele.scrollHeight - ele.scrollTop - jQuery("#" + el).height() < 60;
}
function scrollToBottom() {
        var scrolling = setTimeout(function () {
                var textarea = document.getElementById('trollinner');
                //trollbox.scrollTop(textarea.scrollHeight - jQuery("#trollbox").height());
                textarea.scrollTop = textarea.scrollHeight;
        },500);
}

function makeTrollBox() {
        return openAccordionGroup("Trollbox","TrollOne",false,true) + 
                `<div id="trollbox_container">
                        <div class="summary">Trollbox</div>
                        <div id="trollbox"><div id="trollinner"></div>></div>
                        <input type="text" id="trollsearch" placeholder="Search"></input><br/>
                        <input type="text" id="trollhighlight" placeholder="Highlight"></input>
                </div>` +
                closeAccordionGroup() //<div class="framecover"></div>
}

function setUpTroll() {
        //load the previous chats into the trollbox
        document.getElementById("trollinner").innerHTML = getLs("chats");
        $("#trollsearch").trigger("change")
        trollbox = jQuery("#trollinner");
        $("#trollsearch").val(getLs("search")).trigger('change')
        setTimeout(function () {
                scrollToBottom();
        }, 2000);

        //set click event for name in trollbox to insert into search
        trollbox.on('click', function (e) {
                if (e.target != this) {
                        //clicked a div
                        if (e.target.localName == 'strong') {
                                var name = e.target.textContent.replace(": ", "")
                                $("#trollsearch").val( $("#trollsearch").val() + " " + name)
                                $("#trollsearch").trigger('paste')
                        }
                }
        })
        
        trollbox.dblclick((e)=>{
                if (e.target != this) {
                        var word = window.getSelection().toString()
                        $("#trollsearch").val( $("#trollsearch").val() + " " + word)
                        $("#trollsearch").trigger('paste')
                }
        })

        $("#trollsearch").dblclick(()=>{
            $("#trollsearch").val("").trigger('change')
            scrollToBottom();
        })

        var trollSearch = "a";
        $("#trollsearch").on("change paste keyup", function (e) {
                if (trollSearch == $("#trollsearch").val()){return}
                trollSearch = $("#trollsearch").val();
                clearTimeout(wait);
                var wait = setTimeout(function () {
                        var terms = ""
                        if (trollSearch == "") {
                                $("#trollinner > div").show();
                                if (e.type != "change") {
                                        // if (scrolledBottom){  //scroll to bottom, if it was before we added elements
                                        scrollToBottom();
                                        // }
                                }
                        } else {
                                terms = trollSearch.trim().split(" ");
                                        
                                $("#trollinner > div").hide();
                                
                                $.each(terms, function (index, value) {
                                        $("#trollinner > div:icontains('" + value + "')").show();
                                })

                                scrollToBottom();
                        }
                        setLs("search",trollSearch )
                }, 500)
        });

        $("#trollhighlight").on("change paste keyup", function (e) {
                clearTimeout(wait);
                var wait = setTimeout(function () {
                        if ($("#trollhighlight").val() == "") {
                                $("#trollinner > div").removeClass('highlight');
                        } else {
                                $("#trollinner > div").removeClass('highlight');
                                var terms = $("#trollhighlight").val().trim().split(" ");
                                $.each(terms, function (index, value) {
                                        $("#trollinner > div:icontains('" + value + "')").addClass('highlight');
                                })
                        }
                }, 500)

        });

        $("#trollinner").scroll(function () {
                $(".summary").html("st: " + this.scrollTop + " sh:" + this.scrollHeight + " height:" + $("#trollbox").height() + " - sh-st:" + (this.scrollHeight - this.scrollTop));
                //scrolledBottom = isScrolledToBottom("trollbox");
        });
        // trollbox.scroll(function () {
        //         $("#sc").html("st: " + this.scrollTop + " sh:" + this.scrollHeight + " height:" + $("#trollbox").height() + " - sh-st:" + (this.scrollHeight - this.scrollTop));
        //         //scrolledBottom = isScrolledToBottom("trollbox");
        // });
}