

var pushAPI_URI = "wss://api.poloniex.com";
var connection = new autobahn.Connection({
        url: pushAPI_URI,
        realm: "realm1"
});


//save the pair histories to local storage, every 30 seconds
setInterval(function(){
        // setLs("pairHistories",poloPairs.data.pairHistories,true,false) 
},30000)

var marketData = []
var processingMarketData = []
//process the market event data every 2.5 seconds
setInterval(function(){
        /**
         * sort array by seq number ascending
         * peel off all elements, except the last 50, into new array
         * process the copied array
         */
        marketData.sort(function(a, b) { return parseFloat(a.seq) - parseFloat(b.seq)  });

        if (marketData.length < 51){
                processingMarketData = marketData.slice()
        }else{
                processingMarketData = marketData.splice(0,marketData.length-50)
        }
        
        processingMarketData.forEach(function(item){
                procMarketEvents(item.args,item.seq, item.pairSet)
        })
},2500)
function procMarketEvents(args, seq,pairSet){
        // console.log(pairSet,JSON.stringify(args), kwargs);

                if(typeof initComplete == "undefined" || initComplete == false || args.length == 0){return}

                if (parseFloat(seq) > parseFloat(polo_O.orderBook[pairSet].seq)){polo_O.orderBook[pairSet].seq = seq}else{return}

                args.forEach(function(obj,index){
                        switch (obj.type){
                                case "orderBookRemove":
                                        marketOrderRemove(pairSet,obj.data,seq)
                                        break;
                                case "orderBookModify":
                                        marketOrderModify(pairSet,obj.data,seq)
                                        break;
                                case "newTrade":
                                        marketOrderTrade(pairSet,obj.data,seq)
                                        break;
                        }
                })

                function marketOrderRemove(pSet,data,seq){ 
                        //{"type":"ask","rate":"12.29737283"}{"type":"bid","rate":"12.15001585"}
                        try{
                                var foundIndex

                                if (data.type=="bid"){
                                        dataset = polo_O.orderBook[pSet].bids
                                }else{
                                        dataset = polo_O.orderBook[pSet].asks
                                }
                                foundIndex = dataset.findIndex(x => {return parseFloat(x[0]) == parseFloat(data.rate)})
                                if(foundIndex > -1){
                                        dataset.splice(foundIndex,1)
                                }
                        }catch(e){}
                }
                function marketOrderModify(pSet,data,seq){ 
                        //{"type":"ask","rate":"0.01939544","amount":"51.85120301"}
                        try{
                                var item=[], dataset,found=false, foundIndex, sortFunc;
                                if (data.type=="bid"){
                                        dataset = polo_O.orderBook[pSet].bids
                                        sortFunc = (a,b)=>{return parseFloat(a.rate) - parseFloat(b.rate)}
                                        foundIndex = dataset.findIndex(x => {return parseFloat(x[0]) <= parseFloat(data.rate)})
                                }else{
                                        dataset = polo_O.orderBook[pSet].asks
                                        sortFunc = (a,b)=>{return parseFloat(b.rate) - parseFloat(a.rate)}
                                        foundIndex = dataset.findIndex(x => {return parseFloat(x[0]) >= parseFloat(data.rate)})
                                }
                                
                                found = (dataset[foundIndex][0] == data.rate)
                                if(found){
                                        dataset[foundIndex][1] = parseFloat(data.amount)  //just modify the amount, the rate is the 'key'
                                }else{
                                        item.push(parseFloat(data.rate))
                                        item.push(parseFloat(data.amount))
                                        //dataset.splice(foundIndex,0,item)  //splice in the correct location, so we don't have to sort
                                        dataset.splice(foundIndex,0,item)
                                }
                                // dataset = dataset.sort(sortFunc)
                                
                        }catch(e){}

                }
                function marketOrderTrade(pSet,data,seq){
                        /*{"amount":"0.61671427",       - volume
                        "date":"2017-02-21 06:11:54",
                        "rate":"0.01939544",            - price
                        "total":"0.01196144",           - total sale amount
                        "tradeID":"2713806",
                        "type":"buy"}                   -type  */
                
                }
                //    {    ts: Date.now()/1000,  //.. and append to the beginning of data
                //         ls: args[1],
                //         la: args[2],
                //         hb: args[3],
                //         pc: percentChange, //args[4] TODO store this (24hr change) in the pairHistories object
                //         cuv: args[5],
                //         cov: args[6],
                //         tv: tradeVolume
                //     }

                // [
                //         {"type":"orderBookModify","data":{"type":"ask","rate":"0.01939544","amount":"51.85120301"}},
                //         {"type":"newTrade","data":{"amount":"0.61671427","date":"2017-02-21 06:11:54","rate":"0.01939544","total":"0.01196144","tradeID":"2713806","type":"buy"}}
                // ]

                /*
                USDT_XMR [{"type":"orderBookRemove","data":{"type":"ask","rate":"12.29737283"}}] Object {seq: 86768008}
                pairSet = BTC_CLAM 
                args = [
                                {"type":"orderBookModify","data":{"type":"bid","rate":"0.00089891","amount":"0.74368639"}},  args[0]
                                {"type":"orderBookModify","data":{"type":"bid","rate":"0.00089676","amount":"5.33210927"}}   args[1]
                        ] 
                kwargs = Object {seq: 14308651}  kwargs.seq

                USDT_XMR [{"type":"orderBookRemove","data":{"type":"bid","rate":"12.15001585"}},{"type":"orderBookModify","data":{"type":"bid","rate":"12.15001587","amount":"195.33576469"}}] Object {seq: 86768009}
                BTC_BTS [] Object {seq: 20202023}
                BTC_XMR [{"type":"orderBookRemove","data":{"type":"bid","rate":"0.01119138"}},{"type":"orderBookModify","data":{"type":"bid","rate":"0.01119141","amount":"218.69451659"}}] Object {seq: 138194528}
                BTC_ETH [{"type":"orderBookRemove","data":{"type":"bid","rate":"0.01148507"}},{"type":"orderBookModify","data":{"type":"bid","rate":"0.01149874","amount":"237.21465715"}}] Object {seq: 267484747}
                */
}


connection.onopen = function (session) {

        console.log("Websocket push API connection opened");

        function marketEvent(args,kwargs, pairSet) {
                if (polo_O.orderBook.got == "notyet"){return}
                var seq = kwargs.seq
                marketData.push({args, seq, pairSet})
        }

        function tickerEvent(args, kwargs) {

                if(typeof initComplete == "undefined" || initComplete == false){return}

                showData();
                
                // console.log("Data: " + args)
                // var poloPairs=poloPairs.data.pairSets.length;
                var pairName = args[0]
                var same;
                var percentChange = 0
                var tradeVolume = 0

                //save all pairs data to poloPairs object

                /* 
                do some calculations to prevent storing redundant data, 
                and to store new information.. volume, spread, etc.
                
                later, we can do further analysis to detect a pump on any 
                pairs including those we are not watching
                */
                var pairHistory = null
                try{
                        pairHistory = poloPairs.data.pairHistories.find(x => x.pair == pairName); // see if object with this pairname already exists
                }catch(e){}
                 if(pairHistory){
                        if(pairHistory.data[0].ls == args[1] && pairHistory.data[0].cov == args[6]){
                                //price and volume are the same, don't record
                                // rgs[1] = rgs[1];
                        }else{
                                // if price not the same as last, calculate percent Increase
                                same = (args[1] == pairHistory.data[0].ls) 
                                if (!same) { 
                                        percentChange = percentIncrease(pairHistory.data[0].ls, args[1])
                                }
                                tradeVolume = pairHistory.data[0].cuv - args[5]
                                pairHistory.data.unshift(  {    ts: Date.now()/1000,  //.. and append to the beginning of data
                                                                ls: args[1],
                                                                la: args[2],
                                                                hb: args[3],
                                                                pc: percentChange, //args[4] 
                                                                cuv: args[5],
                                                                cov: args[6],
                                                                tv: tradeVolume
                                                            }
                                                        );
                                // trim the length to the max length
                                if (pairHistory.data.length > dataPairHistoryLength){
                                        pairHistory.data.length = dataPairHistoryLength
                                }
                                
                        }

                        
                 }else{
                       poloPairs.data.pairHistories.push(  //create a new pairHistories child object with this pair name, add to pairHistories
                                {       pair: pairName,
                                        data: [ {
                                                ts: Date.now()/1000,
                                                ls: args[1],
                                                la: args[2],
                                                hb: args[3],
                                                pc: args[4],
                                                cuv: args[5],
                                                cov: args[6]
                                        } ],
                                        totalVolume: 0,
                                }
                        )
                }

                if(window.holdupdates == true){
                        
                }else{
                        if (args[0] == poloPairs.data.pairs[0].name) {
                                // showData();
                                var p = poloPairs.data.pairs[0];
                                updateModel(pair1Model, args, p.name, p.digits);
                        }
                        if (args[0] == poloPairs.data.pairs[1].name) {
                                // showData();
                                var p = poloPairs.data.pairs[1];
                                updateModel(pair2Model, args, p.name, p.digits);
                        }
                        if (args[0] == poloPairs.data.pairs[2].name) {
                                // showData();
                                var p = poloPairs.data.pairs[2];
                                // updateModel(pair3Model, args, p.name, p.digits);
                        }
                        if(!poloPairs.data.pairSets.includes(args[0])){
                                poloPairs.data.pairSets.push(args[0])
                                poloPairs.data.pairSets.sort()
                                // pair3Model.pairs = poloPairs.data.pairSets
                                setLs("pairSets", poloPairs.data.pairSets,true)
                                sendToServer("pairsets", poloPairs.data.pairSets)
                        }
                }
        }

        function trollboxEvent(args, kwargs) {

                if(typeof initComplete == "undefined" || initComplete == false){return}

                procTrollEvent(args);
        }

        //initialize Orderbook updates push listening
        // TODO this seems broken
        // poloPairs.data.pairSets.forEach(function(pairset){
        //      session.subscribe(pairset,function(args,kwargs){
        //              marketEvent(args,kwargs,pairset)(pairset)
        //      });
        // })
                
        // session.subscribe('BTC_NAV', function(args){
        //         marketEvent(args,"BTC_NAV")
        // });

        //session.subscribe('ticker', tickerEvent);
        //session.subscribe('trollbox', trollboxEvent);
}

connection.onclose = function () {
        console.log("Websocket push API connection closed");
}


//connection.open();
// connection2.open();