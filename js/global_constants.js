/***global variables */

//flag to use for script loader knowing if we've already loaded scripts in index file for debugging
var initComplete = false

/**analysis module */
const detectDumpOn = 3;  //consecutive direction changes
const detectPumpOn = 3;

const largeChangeAmount = .1;  //percent change on one tick to classify as big change



/** user options */
const defaultUsername = "Stefano"

/** Order options */
const sellFactor = .85  //   percent range between high bid and low ask
const sellAtRangeFactor = .1; //20% range in either direction
const buyNowFactor = 1.25
const dumpFactor = .75
//global flash object for data input 
var flash;

//holdings and orders count
var holdingsCount, ordersCount

var orderbookLength = 5

/**length of history data for all pairs */
const dataPairHistoryLength = 100;
const histBarTimeLength = 7200; // 120 mins - amount of minutes to show on bar max

/**Pair Vue Models Enum */
var pairModelEnum = {
    pair: 0,
    last: 1,
    lowestAsk: 2,
    highestBid: 3,
    percentChange: 4,
    baseVolume: 5,
    quoteVolume: 6,
    isFrozen: 7,
    low24hr: 8,
    high24hr: 9,
    range24hr: 10,
    WS: 11,
    C: 12,
    PL: 13,
    historyBarLength: 14,
    sameCount: 15
}