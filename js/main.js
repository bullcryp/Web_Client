

var toggled = true;

$(document).ready(function () {

  $("#bc-sidebar").mouseenter(function(){  
    //do all sidebar expansion functions
    $(this).animate({width: "150px"}, "fast");

  }).mouseleave(function(){
    //do all sidebar collapse functions
    $(this).animate({width: "30px"}, "fast");
    
  })

})