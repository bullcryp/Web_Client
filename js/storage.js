
function setLs(name, data, isJSON, showLog){

    if(name=="pairs" || name =="pairSets"){
        if (data.length < 2){
            return
        }
    }

    if(isJSON){data = JSON.stringify(data)}
    data = LZString.compressToBase64(data)
    if(showLog){ console.log("Saved: " + name + " with size of: " + roughSizeOfObject(data)) }
    localStorage.setItem(name, data)

}

function checkForNull(json) {
    for (key in json) {
        if (typeof (json[key]) === "object") {
            return checkForNull(json[key]);
        } else if (json[key] === null) {
            return true;
        }
    }
    return false;
}

function getLs(name, isJSON, showLog){

    try{
        let data = localStorage.getItem(name)

        data = LZString.decompressFromBase64(data) || data
        
        if(name == "pairs" && data == null){
            data = '[{"name":"USDT_BTC","digits":8,"lastUpdated":1522707100181},{"name":"BTC_XMR","digits":8,"lastUpdated":1522707100181},{"name":"BTC_LTC","digits":8,"lastUpdated":1522707100181}]'
        }else if(name == "pairSets" && data == null){
            data = '["BTC_AMP","BTC_ARDR","BTC_BCH","BTC_BCN","BTC_BCY","BTC_BELA","BTC_BLK","BTC_BTCD","BTC_BTM","BTC_BTS","BTC_BURST","BTC_CLAM","BTC_CVC","BTC_DASH","BTC_DCR","BTC_DGB","BTC_DOGE","BTC_EMC2","BTC_ETC","BTC_ETH","BTC_EXP","BTC_FCT","BTC_FLDC","BTC_FLO","BTC_GAME","BTC_GAS","BTC_GNO","BTC_GNT","BTC_GRC","BTC_HUC","BTC_LBC","BTC_LSK","BTC_LTC","BTC_MAID","BTC_NAV","BTC_NEOS","BTC_NMC","BTC_NXC","BTC_NXT","BTC_OMG","BTC_OMNI","BTC_PASC","BTC_PINK","BTC_POT","BTC_PPC","BTC_RADS","BTC_REP","BTC_RIC","BTC_SBD","BTC_SC","BTC_STEEM","BTC_STORJ","BTC_STR","BTC_STRAT","BTC_SYS","BTC_VIA","BTC_VRC","BTC_VTC","BTC_XBC","BTC_XCP","BTC_XEM","BTC_XMR","BTC_XPM","BTC_XRP","BTC_XVC","BTC_ZEC","BTC_ZRX","ETH_BCH","ETH_CVC","ETH_ETC","ETH_GAS","ETH_GNO","ETH_GNT","ETH_LSK","ETH_OMG","ETH_REP","ETH_STEEM","ETH_ZEC","ETH_ZRX","USDT_BCH","USDT_BTC","USDT_DASH","USDT_ETC","USDT_ETH","USDT_LTC","USDT_NXT","USDT_REP","USDT_STR","USDT_XMR","USDT_XRP","USDT_ZEC","XMR_BCN","XMR_BLK","XMR_BTCD","XMR_DASH","XMR_LTC","XMR_MAID","XMR_NXT","XMR_ZEC"]'
        }
        if(isJSON){data = JSON.parse(data)}
        if(showLog){ console.log("Got: " + name + " with size of: " + roughSizeOfObject(data)) }
        return data
    }catch(e){
        return ""
    }


}
