<?php
// written by Alvin Khoo (c) May 2017
// API - returnPair?pair=BTC_EMC2&start=2017-05-20T21:10:00Z&end=2017-05-21T01:10:00Z
// default - returns pair=BTC_ETH --> start = NOW(), end == - 4 hours

$pair = isset($_REQUEST['pair']) ? $_REQUEST['pair'] : 'BTC_ETH' ;

// PDO style
$servername = "localhost";
$username = "polo_user";
$password = "HeaVenLy";
$myDB = "polodb";

try {
    $pdo_conn = new PDO("mysql:host=$servername;dbname=$myDB", $username, $password);
    // set the PDO error mode to exception
    $pdo_conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //echo "Connected successfully $start $end\n<br>";
} catch(PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
    exit(1);
}


$sql = "select MIN(close) as min, MAX(close) as max from `$pair`";


$coin_pairs = array("BTC_AMP","BTC_ARDR","BTC_BCN","BTC_BCY","BTC_BELA","BTC_BLK","BTC_BTCD","BTC_BTM","BTC_BTS","BTC_BURST","BTC_CLAM","BTC_DASH","BTC_DCR","BTC_DGB","BTC_DOGE","BTC_EMC2","BTC_ETC","BTC_ETH","BTC_EXP","BTC_FCT","BTC_FLDC","BTC_FLO","BTC_GAME","BTC_GNO","BTC_GNT","BTC_GRC","BTC_HUC","BTC_LBC","BTC_LSK","BTC_LTC","BTC_MAID","BTC_NAUT","BTC_NAV","BTC_NEOS","BTC_NMC","BTC_NOTE","BTC_NXC","BTC_NXT","BTC_OMNI","BTC_PASC","BTC_PINK","BTC_POT","BTC_PPC","BTC_RADS","BTC_REP","BTC_RIC","BTC_SBD","BTC_SC","BTC_SJCX","BTC_STEEM","BTC_STR","BTC_STRAT","BTC_SYS","BTC_VIA","BTC_VRC","BTC_VTC","BTC_XBC","BTC_XCP","BTC_XEM","BTC_XMR","BTC_XPM","BTC_XRP","BTC_XVC","BTC_ZEC","ETH_ETC","ETH_GNO","ETH_GNT","ETH_LSK","ETH_REP","ETH_STEEM","ETH_ZEC","USDT_BTC","USDT_DASH","USDT_ETC","USDT_ETH","USDT_LTC","USDT_NXT","USDT_REP","USDT_STR","USDT_XMR","USDT_XRP","USDT_ZEC","XMR_BCN","XMR_BLK","XMR_BTCD","XMR_DASH","XMR_LTC","XMR_MAID","XMR_NXT","XMR_ZEC");

// simple check to make sure $pair exists in $coin_pairs
// https://stackoverflow.com/questions/19363634/php-pdo-query-troubleshoot
if (!in_array($pair, $coin_pairs)) {
	return [];
}

//echo $sql;

$myResults = getpair($pdo_conn, $sql, $start, $end);
echo json_encode( $myResults ); // output in [{..trade..}, {..trade2..}, {..}] JSON format

//foreach ($myResults as $key => $value) {
    //echo "{$key} => {$value} ";
//    $output[] = ($value["attributes"]);
//}


$pdo_conn = null; // close() connection;

/* Helper DB function */
function getpair($conn, $query, $start, $end) {
	$stmt = $conn->prepare($query);
	$stmt->execute();
	$row = $stmt->fetchAll(PDO::FETCH_ASSOC);

	// Check if array is empty
	if (empty($row)) { // EMPTY
	    return [];
	} else { // FOUND EMAIL
	    return $row;
	}
}


