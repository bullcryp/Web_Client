<?php
/**
 * Build a simple HTML page with multiple providers.
 */

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include 'vendor/autoload.php';
include 'config.php';

use Hybridauth\Hybridauth;
use Hybridauth\HttpClient;
$hybridauth = new Hybridauth($config);
$adapters = $hybridauth->getConnectedAdapters();
?>

<?php if (!$adapters) : ?>
    <!-- Not logged in -->
<?php endif; ?>


<?php if ($adapters) { 
    if (isset( $_GET['debug']) ) {  ?>
    <ul>
        <?php foreach ($adapters as $name => $adapter) : ?>
            <li>
                <strong><?php print $adapter->getUserProfile()->displayName; ?></strong> from
                <i><?php print $name; ?></i>
                <span>(<a href="<?php print $config['callback'] . "?logout={$name}"; ?>">Log Out</a>)</span><br>
                <i><?php print json_encode( $adapter->getUserProfile() ); ?></i>
                <i><?php print json_encode( $adapter->getAccessToken() ); ?></i>
            </li>
        <?php endforeach; ?>
    </ul>
    <?php
    }else{
        HttpClient\Util::redirect('https://bullcryp.com/trader/index.php');
    }
}; 
?>



<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Oauth login</title>

	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous">
	<!-- Bootstrap core CSS -->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
	<!-- Material Design Bootstrap -->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.0/css/mdb.min.css" rel="stylesheet">

    <style>
        body{
            background-image: url(../images/unsplash_stocks.jpg);
            background-size: cover;
        }
        .login-body {
            margin-top: 30%;
            margin-left: auto;
            color: white;
            text-align: center;
        }
    </style>
</head>
<body>



            <div class="login-body">
            <h5 class="login-title">Please login through a provider below</h5>
                <?php foreach ($hybridauth->getProviders() as $name) : ?>
                <?php if (!isset($adapters[$name])) : ?>
                    <a href="<?php print $config['callback'] . "?provider={$name}"; ?>">
                        <button type="button" class="btn btn-primary">
                            <i class="fab fa-2x <?php echo $config['providers'][$name]['font'] ?>"></i><br>
                            <?php echo $name ?>
                        </button>
                    </a>
                <?php endif; ?>
                <?php endforeach; ?>

            </div>
                                
<!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.13.0/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.0/js/mdb.min.js"></script> -->

</body>
</html>

