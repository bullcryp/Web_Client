const staticAssets = [
  // 'style2.css',
  // 'js/app.js',
  // 'index.html',
  'imgs/unsplash_stocks.jpg'
];

//register the service worker
self.addEventListener('install', async event=>{
  try {
    console.info('install');
    const cache = await caches.open('bullcryp1-static');
    cache.addAll(staticAssets);
    console.info("SW Initialized")
  }catch(e){ 
    console.error(e.message) 
  }
});

//listen for fetch events
self.addEventListener('fetch', event=>{
  const req = event.request;
  const url = new URL(req.url)

  if(req.origin == location.origin){
    event.respondWith(cacheFirst(req))
  }else{
    //event.respondWith(networkFirst(req))
  }
});

async function cacheFirst(req){
  const cachedResponse = await caches.match(req)
  if(cachedResponse){
    console.info("Got Cached Items")
  }else{
    console.info("No cached items")
  }
  return cachedResponse || fetch(req)
}

async function networkFirst(req){
  const cache = await caches.open('news-dynamic')

  try {
    const res = await fetch(req)
    cache.put(req,req.clone() )
    return res
  }catch(e){ 
    return await cache.match(req)
  }
}