<?php
/**
 * Build a configuration array to pass to `Hybridauth\Hybridauth`
 */

$config = [
  /**
   * Set the Authorization callback URL to https://path/to/hybridauth/examples/example_06/callback.php.
   * Understandably, you need to replace 'path/to/hybridauth' with the real path to this script.
   */
  'callback' => 'https://bullcryp.com/trader/callback.php',
  'providers' => [
    'Facebook' => [
      'font' => 'fa-facebook',
      'enabled' => true,
      'keys' => [
        'key' => '1376057272724994',
        'secret' => '7605e82f44d766914835f83e5a980363',
      ],
    ],
    'Twitter' => [
      'font' => 'fa-twitter',
      'enabled' => false,
      'keys' => [
        'id' => 'vojue1xlATaTnc7ELYjGuOENj',
        'secret' => 'Us2DBctp23ofLQyGCmtGNtnSN4LKWiU9914Fa1pDkYi0gVZBW6',
      ],
    ],

    'LinkedIn' => [
      'font' => 'fa-linkedin',
      'enabled' => true,
      'keys' => [
        'id' => '86kev3aai4ca17',
        'secret' => 'Otj90kY4eVZrGS8U',
      ],
    ],
    'Google' => [
      'font' => 'fa-google-plus',
      'enabled' => true,
       //"scope"   => "https://www.googleapis.com/auth/plus.login", // optional
       //                "https://www.googleapis.com/auth/plus.me", // optional
      'keys' => [
        'id' => '54316544318-5472257q8piu9opr04s97ih7vhcui1en.apps.googleusercontent.com',
        'secret' => 'uWfZY1e1hH0p_KF5n-I-ylmF',
      ],
    ],
  ],
];

